/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/connection.h>

#include "basedir.h"
#include "internal.h"
#include "message.h"
#include "path.h"
#include "stream.h"
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

struct keypool_connection_st {
	char *address;
	int fd;
        gnutls_psk_client_credentials_t pskcred;
	struct keypool_stream_st stream;
};

keypool_error_t
keypool_connection_new(keypool_connection_t *connectionp,
		       const char *name,
		       const keypool_buffer_t token)
{
	char *basedir;
	char *address;
	keypool_error_t error = 0;

	basedir = keypool_basedir_get_runtime_directory();
	if (basedir == NULL)
		return KEYPOOL_ERROR_UNKNOWN;

	error = keypool_path_build(&address, '/', basedir, "keypool", NULL);
	free(basedir);
	if (error < 0)
		return error;

	error = keypool_connection_new_for_address(connectionp, name, token,
						   address);
	free(address);
	return error;
}

keypool_error_t
keypool_connection_new_for_address(keypool_connection_t *connectionp,
				   const char *name,
				   const keypool_buffer_t token,
				   const char *address)
{
	keypool_connection_t connection;
	gnutls_datum_t tmp;
	keypool_error_t error = 0;
	struct sockaddr_un sa;
	int ret;

	if (strlen(address) > sizeof(sa.sun_path))
		return KEYPOOL_ERROR_INVALID_REQUEST;

	connection = calloc(1, sizeof(struct keypool_connection_st));
	if (connection == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	connection->address = strdup(address);
	if (connection->address == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	connection->fd = -1;

	ret = gnutls_psk_allocate_client_credentials(&connection->pskcred);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	tmp.data = (uint8_t *)token->data;
	tmp.size = token->size;
	ret = gnutls_psk_set_client_credentials(connection->pskcred,
						name, &tmp,
						GNUTLS_PSK_KEY_RAW);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

 out:
	if (error == 0)
		*connectionp = connection;
	else
		keypool_connection_free(connection);

	return error;
}

void
keypool_connection_free(keypool_connection_t connection)
{
	if (connection != NULL) {
		keypool_stream_deinit(&connection->stream);
		if (connection->stream.session) {
			gnutls_bye(connection->stream.session, GNUTLS_SHUT_RDWR);
			gnutls_deinit(connection->stream.session);
		}
		if (connection->fd >= 0)
			close(connection->fd);
		if (connection->pskcred)
			gnutls_psk_free_client_credentials(connection->pskcred);
		free(connection->address);
	}

	free(connection);
}

static keypool_error_t
keypool_connection_ensure_stream(keypool_connection_t connection)
{
	gnutls_session_t session = NULL;
	keypool_error_t error = 0;
	int ret;

	if (connection->stream.session == NULL) {
		struct sockaddr_un sa;

		memset(&sa, 0, sizeof(sa));
		sa.sun_family = AF_UNIX;
		memcpy(sa.sun_path, connection->address, strlen(connection->address));

		connection->fd = socket(AF_UNIX, SOCK_STREAM, 0);
		if (connection->fd < 0)
			return KEYPOOL_ERROR_UNKNOWN;

		if (connect(connection->fd, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
			error = KEYPOOL_ERROR_UNKNOWN;
			goto out;
		}

		ret = gnutls_init(&session, GNUTLS_CLIENT);
		if (ret < 0) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			goto out;
		}

		ret = gnutls_credentials_set(session, GNUTLS_CRD_PSK,
					     connection->pskcred);
		if (ret < 0) {
			error = KEYPOOL_ERROR_TRANSPORT_ERROR;
			goto out;
		}

		gnutls_transport_set_int(session, connection->fd);
		gnutls_handshake_set_timeout(session,
					     GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
		ret = gnutls_priority_set_direct(session, PRIORITY, NULL);
		if (ret < 0) {
			error = KEYPOOL_ERROR_TRANSPORT_ERROR;
			goto out;
		}

		do {
			ret = gnutls_handshake(session);
		} while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

		if (ret < 0) {
			error = KEYPOOL_ERROR_TRANSPORT_ERROR;
			goto out;
		}

		error = keypool_stream_init(&connection->stream, session);
	}

 out:
	if (error < 0) {
		gnutls_deinit(session);
		gnutls_psk_free_client_credentials(connection->pskcred);
	}
	return error;
}

keypool_error_t
keypool_connection_rekey(keypool_connection_t connection,
			 keypool_buffer_t key,
			 keypool_buffer_t previous_key,
			 keypool_buffer_t tag)
{
	keypool_message_t call = NULL;
	keypool_message_t reply = NULL;
	json_t *json;
	uint8_t keybuf[MAX_KEY_SIZE];
	size_t size;
	keypool_error_t error = 0;
	int ret;

	error = keypool_connection_ensure_stream(connection);
	if (error < 0)
		return error;

	/* Invoke a Varlink call with the signature:
	 * method Get(size: ?int, tag: ?string) -> (tag: string)
	 */

	/* Send the method call. */
	error = keypool_message_call_new(&call, KEYPOOL_NS ".Get");
	if (error < 0)
		return error;

	if (key->size != 0) {
		error = keypool_message_write_size_parameter(call, "size",
							     key->size);
		if (error < 0)
			goto out;
	}

	if (tag->data != NULL) {
		error = keypool_message_write_blob_parameter(call, "tag",
							     tag->data,
							     tag->size);
		if (error < 0)
			goto out;
	}

	error = keypool_message_serialize(call, &json);
	if (error < 0)
		goto out;

	error = keypool_stream_write(&connection->stream, json);
	json_decref(json);
	if (error < 0)
		goto out;

	keypool_message_free(call);
	call = NULL;

	/* Read the reply. */
	error = keypool_stream_read(&connection->stream, &json);
	if (error < 0)
		goto out;

	error = keypool_message_deserialize(&reply, json);
	json_decref(json);
	if (error < 0)
		goto out;

	if (keypool_message_get_type(reply) == KEYPOOL_MESSAGE_TYPE_ERROR) {
		error = KEYPOOL_ERROR_SERVER_OPERATION_FAILED;
		goto out;
	}

	if (tag->data != NULL) {
		size = sizeof(keybuf);
		error = keypool_message_read_blob_parameter(reply, "key",
							    keybuf, &size);
		if (error < 0)
			goto out;
		error = keypool_buffer_set(previous_key, keybuf, size);
		if (error < 0)
			goto out;
	}

	error = keypool_message_read_blob_parameter(reply, "tag",
						    NULL, &size);
	if (error < 0)
		goto out;
	keypool_buffer_deinit(tag);
	error = keypool_buffer_init_sized(tag, size);
	if (error < 0)
		goto out;
	error = keypool_message_read_blob_parameter(reply, "tag",
						    tag->data, &size);
	if (error < 0)
		goto out;
	tag->size = size;

	keypool_message_free(reply);
	reply = NULL;

	/* Export keying material on our side. */
	size = key->size == 0 ? DEFAULT_KEY_SIZE: key->size;
	ret = gnutls_prf_rfc5705(connection->stream.session,
				 sizeof(LABEL) - 1, LABEL,
				 0, NULL,
				 size,
				 (char *)keybuf);
	if (ret < 0) {
		error = KEYPOOL_ERROR_TRANSPORT_ERROR;
		goto out;
	}
	error = keypool_buffer_set(key, keybuf, size);
	if (error < 0)
		goto out;

 out:
	keypool_message_free(call);
	keypool_message_free(reply);

	memset(keybuf, 0, sizeof(keybuf));

	return error;
}
