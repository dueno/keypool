/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "basedir.h"

#include <keypool/error.h>
#include "path.h"
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

static const char * const _keypool_runtime_bases_default[] = { "/run", "/var/run", NULL };
const char * const *_keypool_runtime_bases = _keypool_runtime_bases_default;

char *
keypool_basedir_get_runtime_directory(void)
{
	const char *envvar;
	char *directory;
	struct stat sb;
        struct passwd pwbuf, *pw;
	char buf[1024];
	char *prefix;
	uid_t uid;
	const char * const *bases = _keypool_runtime_bases;
	int i;
	char uidbuf[64];
	int ret;

	envvar = secure_getenv("XDG_RUNTIME_DIR");
	if (envvar != NULL && envvar[0] != '\0')
		return strdup(envvar);

	uid = getuid();
	if (snprintf(uidbuf, sizeof(uidbuf), "%u", uid) >= sizeof(uidbuf))
		return NULL;

	for (i = 0; bases[i] != NULL; i++) {
		ret = keypool_path_build(&prefix, '/', bases[i],
					 "user", uidbuf, NULL);
		if (ret < 0)
                        return NULL;
                if (stat(prefix, &sb) != -1 && S_ISDIR(sb.st_mode))
                        return prefix;
                free(prefix);
        }

	if (getpwuid_r(uid, &pwbuf, buf, sizeof buf, &pw) == 0 &&
            pw != NULL && pw->pw_dir != NULL && *pw->pw_dir == '/') {
		ret = keypool_path_build(&directory, '/', pw->pw_dir,
					 ".cache", NULL);
                if (ret < 0)
                        return NULL;
                return directory;
        }

	return NULL;
}

char *
keypool_basedir_get_config_directory(void)
{
	const char *envvar;
	char *directory;
        struct passwd pwbuf, *pw;
	char buf[1024];
	uid_t uid;
	int ret;

	envvar = secure_getenv("XDG_CONFIG_HOME");
	if (envvar != NULL && envvar[0] != '\0')
		return strdup(envvar);

	uid = getuid();

	if (getpwuid_r(uid, &pwbuf, buf, sizeof buf, &pw) == 0 &&
            pw != NULL && pw->pw_dir != NULL && *pw->pw_dir == '/') {
		ret = keypool_path_build(&directory, '/', pw->pw_dir,
					 ".config", NULL);
                if (ret < 0)
                        return NULL;
                return directory;
        }

	return NULL;
}
