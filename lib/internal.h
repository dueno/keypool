/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_INTERNAL_H_
#define KEYPOOL_INTERNAL_H_

#include <gnutls/abstract.h>
#include <keypool/credentials.h>

#define KEYPOOL_NS "org.gnome.Keypool"

#define LABEL "keypool"
#define MAX_KEY_SIZE 256
#define DEFAULT_KEY_SIZE 64
#define MAC_SIZE 32
#define PRIORITY "NORMAL:+VERS-TLS1.3:+DHE-PSK"

keypool_error_t _keypool_credentials_get(keypool_credentials_t credentials,
					 gnutls_privkey_t privkey,
					 const char *name,
					 keypool_buffer_t token);

#endif	/* KEYPOOL_INTERNAL_H_ */
