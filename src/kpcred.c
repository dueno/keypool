/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/credentials.h>
#include <getopt.h>
#include <gnutls/crypto.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static struct option long_options[] = {
	{ "credentials", required_argument, 0, CHAR_MAX + 'c' },
	{ "pubkey", required_argument, 0, CHAR_MAX + 'p' },
	{ "name", required_argument, 0, CHAR_MAX + 'n' },
	{ "help", no_argument, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static void
usage(FILE *out)
{
	fprintf(out,
		"Usage: kpcred {register|unregister} [OPTIONS]\n"
		"where OPTIONS are:\n"
		"  --credentials=FILE      specify the credentials file to authenticate clients\n"
		"  --pubkey=FILE           specify the public key file\n"
		"  --name=NAME             specify the name of authentication token\n"
		"  -h, --help              give this help list\n");
}

static keypool_error_t
keypool_tool_register(const char *credentials_file,
		      const char *pubkey_file,
		      const char *name)
{
	keypool_credentials_t credentials;
	struct keypool_buffer_st token;
	keypool_error_t error = 0;
	uint8_t buffer[32];
	gnutls_datum_t tmp, hex;
	int ret;

	error = keypool_credentials_new(&credentials);
	if (error < 0) {
		fprintf(stderr, "failed to allocate credentials\n");
		return error;
	}

	error = keypool_credentials_load(credentials, credentials_file);
	if (error < 0 && error != KEYPOOL_ERROR_FILE_NOT_FOUND) {
		fprintf(stderr, "failed to load credentials\n");
		goto out;
	}

	ret = gnutls_rnd(GNUTLS_RND_KEY, buffer, sizeof(buffer));
	if (ret < 0) {
		fprintf(stderr, "failed to generate key: %s\n",
			gnutls_strerror(ret));
		error = KEYPOOL_ERROR_SERVER_OPERATION_FAILED;
		goto out;
	}

	keypool_buffer_init_borrowed(&token, buffer, sizeof(buffer));
	error = keypool_credentials_set(credentials, pubkey_file,
					name, &token);
	if (error < 0) {
		fprintf(stderr, "failed to set credentials\n");
		goto out;
	}

	error = keypool_credentials_save(credentials, credentials_file);
	if (error < 0) {
		fprintf(stderr, "failed to save credentials\n");
		goto out;
	}

	tmp.data = token.data;
	tmp.size = token.size;
	ret = gnutls_hex_encode2(&tmp, &hex);
	if (ret < 0) {
		fprintf(stderr, "failed to encode token: %s\n",
			gnutls_strerror(ret));
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
	printf("%s is registered with token: %s\n", name, (char *)hex.data);
	gnutls_free(hex.data);

 out:
	keypool_credentials_unref(credentials);
	return error;
}

static keypool_error_t
keypool_tool_unregister(const char *credentials_file,
			const char *name)
{
	keypool_credentials_t credentials;
	keypool_error_t error = 0;

	error = keypool_credentials_new(&credentials);
	if (error < 0) {
		fprintf(stderr, "failed to allocate credentials\n");
		return error;
	}

	error = keypool_credentials_load(credentials, credentials_file);
	if (error < 0) {
		fprintf(stderr, "failed to load credentials from %s\n",
			credentials_file);
		goto out;
	}

	error = keypool_credentials_unset(credentials, name);
	if (error < 0) {
		fprintf(stderr, "failed to unset credentials\n");
		goto out;
	}

	error = keypool_credentials_save(credentials, credentials_file);
	if (error < 0) {
		fprintf(stderr, "failed to save credentials\n");
		goto out;
	}

 out:
	keypool_credentials_unref(credentials);
	return error;
}

int
main(int argc, char **argv)
{
	const char *credentials = NULL, *name = NULL, *pubkey = NULL;
	keypool_error_t error;

	while (1) {
		int c, option_index = 0;

		c = getopt_long(argc, argv, "h", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			usage(stdout);
			return EXIT_SUCCESS;
		case CHAR_MAX + 'c':
			credentials = optarg;
			break;
		case CHAR_MAX + 'n':
			name = optarg;
			break;
		case CHAR_MAX + 'p':
			pubkey = optarg;
			break;
		default:
			usage(stderr);
			exit(EXIT_FAILURE);
		}
	}
	if (optind == argc) {
		usage(stderr);
		exit(EXIT_FAILURE);
	}
	if (strcmp(argv[optind], "register") == 0) {
		if (credentials == NULL || pubkey == NULL || name == NULL) {
			fprintf(stderr, "--credentials, --pubkey, and --name must be specified\n");
			exit(EXIT_FAILURE);
		}
		error = keypool_tool_register(credentials, pubkey, name);
	} else if (strcmp(argv[optind], "unregister") == 0) {
		if (credentials == NULL || name == NULL) {
			fprintf(stderr, "--credentials and --name must be specified\n");
			exit(EXIT_FAILURE);
		}
		error = keypool_tool_unregister(credentials, name);
	}
	if (error < 0)
		exit(EXIT_FAILURE);

	return EXIT_SUCCESS;
}
