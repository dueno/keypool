/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/connection.h>
#include <getopt.h>
#include <gnutls/gnutls.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ANSI_BOLD_ON "\x1b[1m"
#define ANSI_BOLD_OFF "\x1b[22m"

static struct option long_options[] = {
	{ "key-size", required_argument, 0, CHAR_MAX + 'k' },
	{ "name", required_argument, 0, CHAR_MAX + 'n' },
	{ "socket", required_argument, 0, CHAR_MAX + 's' },
	{ "token", required_argument, 0, CHAR_MAX + 't' },
	{ "tag", required_argument, 0, CHAR_MAX + 'T' },
	{ "help", no_argument, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static void
usage(FILE *out)
{
	fprintf(out,
		"Usage: kpcli [OPTIONS]\n"
		"where OPTIONS are:\n"
		"  --socket=ADDRESS        specify the socket address to connect\n"
		"  --name=NAME             specify the name of authentication token\n"
		"  --token=TOKEN           specify the hex-encoded token\n"
		"  --tag=TAG               specify previously given tag\n"
		"  --key-size=SIZE         specify the size of key to generate\n"
		"  -h, --help              give this help list\n");
}

static keypool_error_t
keypool_tool_rekey(const char *address,
		   const char *name, const char *token_hex,
		   size_t key_size, const char *tag_b64)
{
	struct keypool_buffer_st token = KEYPOOL_BUFFER_INIT;
	struct keypool_buffer_st key = KEYPOOL_BUFFER_INIT;
	struct keypool_buffer_st previous_key = KEYPOOL_BUFFER_INIT;
	struct keypool_buffer_st tag = KEYPOOL_BUFFER_INIT;
	keypool_connection_t connection = NULL;
	gnutls_datum_t tmp, buffer;
	uint8_t buf[1024];
	char hexbuf[1024];
	size_t size;
	keypool_error_t error = 0;
	int ret;

	tmp.data = (uint8_t *)token_hex;
	tmp.size = strlen(token_hex);

	size = sizeof(buf);
	ret = gnutls_hex_decode(&tmp, buf, &size);
	if (ret < 0) {
		error = KEYPOOL_ERROR_INVALID_REQUEST;
		fprintf(stderr, "cannot decode token: %s\n", token_hex);
		goto out;
	}

	error = keypool_buffer_init(&token, buf, size);
	if (error < 0)
		goto out;

	if (tag_b64 != NULL) {
		tmp.data = (uint8_t *)tag_b64;
		tmp.size = strlen(tag_b64);

		ret = gnutls_base64_decode2(&tmp, &buffer);
		if (ret < 0) {
			error = KEYPOOL_ERROR_INVALID_REQUEST;
			fprintf(stderr, "cannot decode tag: %s\n", tag_b64);
			goto out;
		}

		error = keypool_buffer_init(&tag, buffer.data, buffer.size);
		gnutls_free(buffer.data);
		if (error < 0)
			goto out;
	}

	error = keypool_buffer_init_sized(&key, key_size);
	if (error < 0)
		goto out;

	keypool_buffer_init_borrowed(&previous_key, buf, sizeof(buf));

	if (address == NULL)
		error = keypool_connection_new(&connection, name, &token);
	else
		error = keypool_connection_new_for_address(&connection, name,
							   &token, address);
	if (error < 0)
		goto out;

	error = keypool_connection_rekey(connection, &key, &previous_key, &tag);
	if (error < 0) {
		fprintf(stderr, "cannot generate key: %d\n", ret);
		goto out;
	}

	if (tag_b64 != NULL) {
		tmp.data = (uint8_t *)keypool_buffer_get(&previous_key, &size);
		tmp.size = size;
		size = sizeof(hexbuf);
		ret = gnutls_hex_encode(&tmp, hexbuf, &size);
		if (ret < 0) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			fprintf(stderr, "cannot encode old key: %s\n",
				gnutls_strerror(ret));
			goto out;
		}
		printf(ANSI_BOLD_ON "Previous key:" ANSI_BOLD_OFF " %s\n", hexbuf);
		printf(ANSI_BOLD_ON "Previous tag:" ANSI_BOLD_OFF " %s\n", tag_b64);
	}

	tmp.data = (uint8_t *)keypool_buffer_get(&key, &size);
	tmp.size = size;
	size = sizeof(hexbuf);
	ret = gnutls_hex_encode(&tmp, hexbuf, &size);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		fprintf(stderr, "cannot encode new key: %s\n",
			gnutls_strerror(ret));
		goto out;
	}
	printf(ANSI_BOLD_ON "Key:" ANSI_BOLD_OFF " %s\n", hexbuf);

	tmp.data = (uint8_t *)keypool_buffer_get(&tag, &size);
	tmp.size = size;
	size = sizeof(hexbuf);
	ret = gnutls_base64_encode2(&tmp, &buffer);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		fprintf(stderr, "cannot encode new tag: %s\n",
			gnutls_strerror(ret));
		goto out;
	}
	printf(ANSI_BOLD_ON "Tag:" ANSI_BOLD_OFF " %s\n", (char *)buffer.data);
	gnutls_free(buffer.data);

 out:
	keypool_buffer_deinit(&token);
	keypool_buffer_deinit(&key);
	keypool_buffer_deinit(&previous_key);
	keypool_buffer_deinit(&tag);
	keypool_connection_free(connection);

	return error;
}

int
main(int argc, char **argv)
{
	const char *address = NULL, *name = NULL, *token = NULL, *tag = NULL;
	size_t key_size = 64;
	keypool_error_t error;

	while (1) {
		int c, option_index = 0;

		c = getopt_long(argc, argv, "h", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			usage(stdout);
			return EXIT_SUCCESS;
		case CHAR_MAX + 'k':
			key_size = atoi(optarg);
			break;
		case CHAR_MAX + 'n':
			name = optarg;
			break;
		case CHAR_MAX + 's':
			address = optarg;
			break;
		case CHAR_MAX + 't':
			token = optarg;
			break;
		case CHAR_MAX + 'T':
			tag = optarg;
			break;
		default:
			usage(stderr);
			exit(EXIT_FAILURE);
		}
	}

	if (name == NULL || token == NULL) {
		fprintf(stderr, "--name and --token must be specified\n");
		exit(EXIT_FAILURE);
	}

	error = keypool_tool_rekey(address, name, token, key_size, tag);
	if (error < 0) {
		fprintf(stderr, "couldn't perform rekey\n");
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
