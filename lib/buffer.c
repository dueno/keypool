/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/buffer.h>

#include <stdlib.h>
#include <string.h>

keypool_error_t
keypool_buffer_init(keypool_buffer_t buffer,
		    const uint8_t *data, size_t size)
{
	buffer->data = malloc(size);
	if (buffer->data == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;
	memcpy(buffer->data, data, size);
	buffer->size = size;
	buffer->capacity = size;
	buffer->allocated = true;

	return 0;
}

void
keypool_buffer_init_borrowed(keypool_buffer_t buffer,
			     uint8_t *data, size_t size)
{
	buffer->data = data;
	buffer->size = size;
	buffer->capacity = size;
	buffer->allocated = false;
}

keypool_error_t
keypool_buffer_init_sized(keypool_buffer_t buffer,
			  size_t size)
{
	buffer->data = malloc(size);
	if (buffer->data == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;
	buffer->size = 0;
	buffer->capacity = size;
	buffer->allocated = true;
	return 0;
}

keypool_error_t
keypool_buffer_set(keypool_buffer_t buffer,
		   const uint8_t *data, size_t size)
{
	if (buffer->allocated) {
		if (size > buffer->capacity) {
			buffer->data = realloc(buffer->data, size);
			if (buffer->data == NULL)
				return KEYPOOL_ERROR_NO_MEMORY;
			buffer->capacity = size;
		}
	} else if (size > buffer->capacity)
		return KEYPOOL_ERROR_BUFFER_TOO_SHORT;
	memcpy(buffer->data, data, size);
	buffer->size = size;
	return 0;
}

const uint8_t *
keypool_buffer_get(keypool_buffer_t buffer, size_t *size)
{
	*size = buffer->size;
	return buffer->data;
}

void
keypool_buffer_deinit(keypool_buffer_t buffer)
{
	if (buffer && buffer->allocated)
		free(buffer->data);
}
