/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_DEBUG_H_
#define KEYPOOL_DEBUG_H_

#undef keypool_debug
#define keypool_debug(format, ...) do { \
		keypool_log (10, "%s: " format, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
	} while (0)

#undef keypool_audit_log
#define keypool_audit_log(format, ...) do { \
		keypool_log (0, "%s: " format, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
	} while (0)

void keypool_log(int level, const char *format, ...);

#endif	/* KEYPOOL_DEBUG_H_ */
