/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_ERROR_H_
#define KEYPOOL_ERROR_H_

typedef enum {
	KEYPOOL_ERROR_UNKNOWN = -1,
	KEYPOOL_ERROR_NO_MEMORY = -2,
	KEYPOOL_ERROR_INTERNAL_ERROR = -3,
	KEYPOOL_ERROR_TRANSPORT_ERROR = -4,
	KEYPOOL_ERROR_PROTOCOL_ERROR = -5,
	KEYPOOL_ERROR_INVALID_MESSAGE = -6,
	KEYPOOL_ERROR_INVALID_REQUEST = -7,
	KEYPOOL_ERROR_NOT_IMPLEMENTED = -8,
	KEYPOOL_ERROR_INVALID_FILE = -9,
	KEYPOOL_ERROR_SERVER_OPERATION_FAILED = -10,
	KEYPOOL_ERROR_DATA_NOT_AVAILABLE = -11,
	KEYPOOL_ERROR_BUFFER_TOO_SHORT = -12,
	KEYPOOL_ERROR_DECRYPTION_FAILURE = -13,
	KEYPOOL_ERROR_FILE_ERROR = -14,
	KEYPOOL_ERROR_FILE_NOT_FOUND = -15,
	KEYPOOL_ERROR_TRANSPORT_EOF = -16
} keypool_error_t;

#endif	/* KEYPOOL_ERROR_H_ */
