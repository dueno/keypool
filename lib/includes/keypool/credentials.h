/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_CREDENTIALS_H_
#define KEYPOOL_CREDENTIALS_H_

#include <keypool/buffer.h>

typedef struct keypool_credentials_st *keypool_credentials_t;

keypool_error_t keypool_credentials_new(keypool_credentials_t *credentials);
keypool_error_t keypool_credentials_load(keypool_credentials_t credentials,
					 const char *file);
keypool_error_t keypool_credentials_save(keypool_credentials_t credentials,
					 const char *file);
keypool_error_t keypool_credentials_set(keypool_credentials_t credentials,
					const char *pubkey,
					const char *name,
					keypool_buffer_t token);
keypool_error_t keypool_credentials_unset(keypool_credentials_t credentials,
					  const char *name);

keypool_credentials_t keypool_credentials_ref(keypool_credentials_t credentials);
void keypool_credentials_unref(keypool_credentials_t credentials);

#endif	/* KEYPOOL_CREDENTIALS_H_ */
