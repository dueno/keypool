/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_MESSAGE_H_
#define KEYPOOL_MESSAGE_H_

#include <keypool/error.h>
#include <jansson.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

typedef enum {
	KEYPOOL_MESSAGE_TYPE_UNKNOWN,
	KEYPOOL_MESSAGE_TYPE_CALL,
	KEYPOOL_MESSAGE_TYPE_REPLY,
	KEYPOOL_MESSAGE_TYPE_ERROR
} keypool_message_type_t;

typedef struct keypool_message_st *keypool_message_t;

keypool_error_t keypool_message_call_new(keypool_message_t *messagep,
					 const char *name);
keypool_error_t keypool_message_reply_new(keypool_message_t *messagep);
keypool_error_t keypool_message_error_new(keypool_message_t *messagep,
					  const char *name);

keypool_message_type_t keypool_message_get_type(keypool_message_t message);
const char *keypool_message_get_name(keypool_message_t message);

void keypool_message_free(keypool_message_t message);

keypool_error_t keypool_message_write_size_parameter(keypool_message_t message,
						     const char *name,
						     size_t size);
ssize_t keypool_message_read_size_parameter(keypool_message_t message,
					    const char *name);

keypool_error_t keypool_message_write_blob_parameter(keypool_message_t message,
						     const char *name,
						     const uint8_t *blob,
						     size_t blob_size);
keypool_error_t keypool_message_read_blob_parameter(keypool_message_t message,
						    const char *name,
						    uint8_t *blob,
						    size_t *blob_size);

keypool_error_t keypool_message_serialize(keypool_message_t message,
					  json_t **json);
keypool_error_t keypool_message_deserialize(keypool_message_t *messagep,
					    json_t *json);

#endif	/* KEYPOOL_MESSAGE_H_ */
