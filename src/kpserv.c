/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/service.h>
#include <assert.h>
#include <errno.h>
#include <gnutls/crypto.h>
#include <getopt.h>
#include <limits.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

static struct option long_options[] = {
	{ "credentials", required_argument, 0, CHAR_MAX + 'c' },
	{ "privkey", required_argument, 0, CHAR_MAX + 'P' },
	{ "pubkey", required_argument, 0, CHAR_MAX + 'p' },
	{ "socket", required_argument, 0, CHAR_MAX + 's' },
	{ "help", no_argument, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static void
usage(FILE *out)
{
	fprintf(out,
		"Usage: kpserv [OPTIONS]\n"
		"where OPTIONS are:\n"
		"  --socket=ADDRESS        specify the socket address to connect\n"
		"  --credentials=FILE      specify the credentials file to authenticate clients\n"
		"  --pubkey=FILE           specify the public key file\n"
		"  --privkey=FILE          specify the private key file\n"
		"  -h, --help              give this help list\n");
}

static char *socket_file;

static void
remove_socket_file(void)
{
	if (socket_file)
		remove(socket_file);
}

static void
remove_socket_file_on_signal(int sig)
{
	(void)sig;
	remove_socket_file();
}

static keypool_error_t
pin_callback(void *data, const char *prompt, char *pin, size_t pin_max)
{
	char *pass;
	keypool_error_t error = 0;

	(void)data;
	pass = getpass(prompt);
	if (pass == NULL)
		return KEYPOOL_ERROR_INVALID_REQUEST;
	if (strlen(pass) + 1 > pin_max) {
		error = KEYPOOL_ERROR_BUFFER_TOO_SHORT;
		goto out;
	}

 out:
	if (error == 0)
		strcpy(pin, pass);

	free(pass);
	return error;
}

static keypool_error_t
keypool_tool_start_service(const char *address, const char *credentials_file,
			   const char *pubkey, const char *privkey)
{
	keypool_credentials_t credentials = NULL;
	keypool_service_t service = NULL;
	struct sigaction sa, old_sa;
	int listen_fd = -1;
	struct pollfd *fds = NULL;
	nfds_t nfds = 0;
	keypool_error_t error = 0;
	int ret;

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = remove_socket_file_on_signal;
	sigemptyset (&sa.sa_mask);
	sigaction(SIGINT, NULL, &old_sa);
	if (old_sa.sa_handler != SIG_IGN)
		sigaction(SIGINT, &sa, NULL);
	sigaction(SIGHUP, NULL, &old_sa);
	if (old_sa.sa_handler != SIG_IGN)
		sigaction(SIGHUP, &sa, NULL);
	sigaction(SIGTERM, NULL, &old_sa);
	if (old_sa.sa_handler != SIG_IGN)
		sigaction(SIGTERM, &sa, NULL);

	atexit(remove_socket_file);

	error = keypool_credentials_new(&credentials);
	if (error < 0) {
		fprintf(stderr, "failed to allocate credentials\n");
		return error;
	}

	error = keypool_credentials_load(credentials, credentials_file);
	if (error < 0) {
		fprintf(stderr, "failed to load credentials from %s\n",
			credentials_file);
		return error;
	}

	error = keypool_service_new(&service, credentials, pubkey, privkey);
	if (error < 0) {
		fprintf(stderr, "failed to allocate service\n");
		goto out;
	}
	keypool_service_set_pin_function(service, pin_callback, NULL);

	if (address == NULL)
		ret = keypool_service_listen(service);
	else
		ret = keypool_service_listen_on_address(service, address);
	if (ret < 0) {
		fprintf(stderr, "cannot listen on the socket\n");
		goto out;
	}

	address = keypool_service_get_address(service);
	assert(address != NULL);
	socket_file = strdup(address);

	listen_fd = ret;
	fds = realloc(fds, (nfds + 1) * sizeof(struct pollfd));
	if (fds == NULL) {
		fprintf(stderr, "cannot allocate memory\n");
		ret = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
	fds[nfds].fd = listen_fd;
	fds[nfds].events = POLLIN | POLLERR | POLLHUP;
	fds[nfds].revents = 0;
	nfds++;

	while (1) {
		nfds_t i;
		struct sockaddr_un peer_addr;
		socklen_t peer_addr_size;

		ret = poll(fds, nfds, -1);
		if (ret < 0) {
			fprintf(stderr, "failed to poll: %s\n", strerror(errno));
			ret = KEYPOOL_ERROR_PROTOCOL_ERROR;
			break;
		}
		if (ret == 0)
			continue;

		if (fds[0].revents & POLLIN) {
			peer_addr_size = sizeof(struct sockaddr_un);
			ret = accept(listen_fd, &peer_addr, &peer_addr_size);
			if (ret < 0) {
				fprintf(stderr, "failed to accept: %s\n",
					strerror(errno));
				continue;
			}
			fds = realloc(fds, (nfds + 1) * sizeof(struct pollfd));
			if (fds == NULL) {
				fprintf(stderr, "cannot allocate memory\n");
				continue;
			}
			fds[nfds].fd = ret;
			fds[nfds].events = POLLIN | POLLERR | POLLHUP;
			fds[nfds].revents = 0;
			nfds++;
		}

		for (i = 1; i < nfds; i++) {
			if (fds[i].revents & POLLIN) {
				error = keypool_service_dispatch(service, fds[i].fd);
				if (error < 0) {
					if (error != KEYPOOL_ERROR_TRANSPORT_EOF)
						fprintf(stderr, "failed to dispatch %d\n",
							fds[i].fd);
					close(fds[i].fd);
					fds[i].fd = -1;
				}
			} else if (fds[i].revents & (POLLERR | POLLHUP)) {
				close(fds[i].fd);
				fds[i].fd = -1;
			}
			fds[i].revents = 0;
		}
	}
 out:
	if (listen_fd >= 0)
		close(listen_fd);
	keypool_service_free(service);
	keypool_credentials_unref(credentials);

	return error;
}

int
main(int argc, char **argv)
{
	const char *address = NULL, *pubkey = NULL, *privkey = NULL;
	const char *credentials = NULL;
	keypool_error_t error;

	while (1) {
		int c, option_index = 0;

		c = getopt_long(argc, argv, "h", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			usage(stdout);
			return EXIT_SUCCESS;
		case CHAR_MAX + 'c':
			credentials = optarg;
			break;
		case CHAR_MAX + 'p':
			pubkey = optarg;
			break;
		case CHAR_MAX + 'P':
			privkey = optarg;
			break;
		case CHAR_MAX + 's':
			address = optarg;
			break;
		default:
			usage(stderr);
			exit(EXIT_FAILURE);
		}
	}

	if (credentials == NULL || privkey == NULL) {
		fprintf(stderr, "--credentials and --privkey must be specified\n");
		exit(EXIT_FAILURE);
	}

	error = keypool_tool_start_service(address, credentials, pubkey, privkey);
	if (error < 0)
		exit(EXIT_FAILURE);

	return EXIT_SUCCESS;
}
