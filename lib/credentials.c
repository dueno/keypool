/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/credentials.h>
#include <gnutls/abstract.h>
#include <jansson.h>

#if HAVE_ATOMIC_BUILTINS
#define INCREF(obj) __atomic_add_fetch(&obj->refcount, 1, __ATOMIC_ACQUIRE)
#define DECREF(obj) __atomic_sub_fetch(&obj->refcount, 1, __ATOMIC_RELEASE)
#else
#define INCREF(obj) (++obj->refcount)
#define DECREF(obj) (--obj->refcount)
#endif

struct keypool_credentials_st
{
	json_t *json;
	volatile size_t refcount;
};

static void
keypool_credentials_free(keypool_credentials_t credentials)
{
	json_decref(credentials->json);
	free(credentials);
}

keypool_error_t
keypool_credentials_new(keypool_credentials_t *credentialsp)
{
	keypool_credentials_t credentials;
	keypool_error_t error = 0;

	credentials = calloc(1, sizeof(struct keypool_credentials_st));
	if (credentials == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	credentials->json = json_object();
	if (credentials->json == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	credentials->refcount = 1;

 out:
	if (error == 0)
		*credentialsp = credentials;
	else
		keypool_credentials_free(credentials);
	return error;
}

keypool_credentials_t
keypool_credentials_ref(keypool_credentials_t credentials)
{
	INCREF(credentials);
	return credentials;
}

void
keypool_credentials_unref(keypool_credentials_t credentials)
{
	if (credentials == NULL)
		return;
	if (DECREF(credentials) == 0)
		keypool_credentials_free(credentials);
}

keypool_error_t
keypool_credentials_load(keypool_credentials_t credentials,
			 const char *file)
{
	json_t *root;
	keypool_error_t error = 0;
	json_error_t json_error;

	root = json_load_file(file, 0, &json_error);
	if (root == NULL) {
		if (json_error_code(&json_error) == json_error_cannot_open_file)
			return KEYPOOL_ERROR_FILE_NOT_FOUND;
		return KEYPOOL_ERROR_INVALID_FILE;
	}

	if (!json_is_object(root)) {
		json_decref(root);
		return KEYPOOL_ERROR_INVALID_FILE;
	}
	json_object_update(credentials->json, root);

	return error;
}

keypool_error_t
keypool_credentials_save(keypool_credentials_t credentials,
			 const char *file)
{
	int ret;

	ret = json_dump_file(credentials->json, file, 0);
	if (ret < 0)
		return KEYPOOL_ERROR_SERVER_OPERATION_FAILED;

	return 0;
}

keypool_error_t
_keypool_credentials_get(keypool_credentials_t credentials,
			 gnutls_privkey_t privkey,
			 const char *name,
			 keypool_buffer_t token)
{
	json_t *value;
	gnutls_datum_t tmp, buffer;
	keypool_error_t error;
	int ret;

	value = json_object_get(credentials->json, name);
	if (value == NULL)
		return KEYPOOL_ERROR_DATA_NOT_AVAILABLE;
	if (!json_is_string(value))
		return KEYPOOL_ERROR_INVALID_FILE;

	tmp.data = (uint8_t *)json_string_value(value);
	tmp.size = json_string_length(value);
	ret = gnutls_base64_decode2(&tmp, &buffer);
	if (ret < 0)
		return KEYPOOL_ERROR_INVALID_FILE;
	ret = gnutls_privkey_decrypt_data(privkey, 0, &buffer, &tmp);
	gnutls_free(buffer.data);
	if (ret < 0)
		return KEYPOOL_ERROR_DECRYPTION_FAILURE;
	error = keypool_buffer_init(token, tmp.data, tmp.size);
	gnutls_free(tmp.data);
	return error;
}

keypool_error_t
keypool_credentials_set(keypool_credentials_t credentials,
			const char *pubkey_file,
			const char *name,
			keypool_buffer_t token)
{
	gnutls_datum_t tmp, buffer;
	gnutls_pubkey_t pubkey = NULL;
	json_t *value = NULL;
	keypool_error_t error = 0;
	int ret;

	ret = gnutls_pubkey_init(&pubkey);
	if (ret < 0)
		return KEYPOOL_ERROR_NO_MEMORY;

	ret = gnutls_load_file(pubkey_file, &buffer);
	if (ret < 0) {
		error = KEYPOOL_ERROR_FILE_ERROR;
		goto out;
	}
	ret = gnutls_pubkey_import(pubkey, &buffer, GNUTLS_X509_FMT_PEM);
	gnutls_free(buffer.data);
	if (ret < 0) {
		error = KEYPOOL_ERROR_INVALID_FILE;
		goto out;
	}

	tmp.data = token->data;
	tmp.size = token->size;
	ret = gnutls_pubkey_encrypt_data(pubkey, 0, &tmp, &buffer);
	if (ret < 0) {
		error = KEYPOOL_ERROR_DECRYPTION_FAILURE;
		goto out;
	}
	ret = gnutls_base64_encode2(&buffer, &tmp);
	gnutls_free(buffer.data);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	value = json_string((char *)tmp.data);
	gnutls_free(tmp.data);
	if (value == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
	ret = json_object_set_new(credentials->json, name, value);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

 out:
	if (error < 0)
		json_decref(value);
	if (pubkey != NULL)
		gnutls_pubkey_deinit(pubkey);
	return error;
}

keypool_error_t
keypool_credentials_unset(keypool_credentials_t credentials,
			  const char *name)
{
	int ret;

	ret = json_object_del(credentials->json, name);
	if (ret < 0)
		return KEYPOOL_ERROR_DATA_NOT_AVAILABLE;
	return 0;
}
