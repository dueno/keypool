/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "stream.h"

#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/signal.h>

#define CONNECTION_BUFFER_SIZE (16 * 1024 * 1024)

keypool_error_t
keypool_stream_init(keypool_stream_t stream,
		    gnutls_session_t session)
{
	keypool_error_t error = 0;

	memset(stream, 0, sizeof(struct keypool_stream_st));

        stream->in = malloc(CONNECTION_BUFFER_SIZE);
        if (!stream->in) {
                error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

        stream->out = malloc(CONNECTION_BUFFER_SIZE);
        if (!stream->out) {
                error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	stream->session = session;

 out:
	if (error < 0)
		keypool_stream_deinit(stream);

        return error;
}

void
keypool_stream_deinit(keypool_stream_t stream) {
        free(stream->in);
        free(stream->out);
}

static keypool_error_t
keypool_stream_flush(keypool_stream_t stream)
{
	ptrdiff_t to_send = stream->out_end - stream->out_start;
	ssize_t ret;

	do {
		ret = gnutls_record_send(stream->session,
					 stream->out + stream->out_start,
					 to_send);
		if (ret < 0)
			return KEYPOOL_ERROR_TRANSPORT_ERROR;

		if (ret < to_send)
			stream->out_start += ret;
		to_send -= ret;
	} while (to_send > 0);

	stream->out_start = 0;
	stream->out_end = 0;
	return 0;
}

ssize_t
keypool_stream_read(keypool_stream_t stream, json_t **objectp)
{
        for (;;) {
                uint8_t *nul;
		ssize_t ret;

                nul = memchr(&stream->in[stream->in_start], 0, stream->in_end - stream->in_start);
                if (nul) {
			json_t *object;

                        object = json_loads((const char *)&stream->in[stream->in_start], 0, NULL);
                        if (object == NULL) {
				*objectp = NULL;
				return KEYPOOL_ERROR_INVALID_MESSAGE;
			}
			if (!json_is_object(object)) {
				json_decref(object);
				*objectp = NULL;
				return KEYPOOL_ERROR_INVALID_MESSAGE;
			}

			*objectp = object;
                        stream->in_start = (nul + 1) - stream->in;
                        return 1;
                }

		memmove(stream->in,
			stream->in + stream->in_start,
			stream->in_end - stream->in_start);
		stream->in_end = stream->in_end - stream->in_start;
		stream->in_start = 0;

		ret = gnutls_record_recv(stream->session,
					 stream->in + stream->in_end,
					 CONNECTION_BUFFER_SIZE - stream->in_end);
		if (ret < 0) {
			*objectp = NULL;
			return KEYPOOL_ERROR_TRANSPORT_ERROR;
		}

		if (ret == 0) {
			*objectp = NULL;
			return 0;
		}

		stream->in_end += ret;

                if (stream->in_end == CONNECTION_BUFFER_SIZE) {
			*objectp = NULL;
                        return KEYPOOL_ERROR_INVALID_MESSAGE;
		}
        }

        /* should not be reached */
        return KEYPOOL_ERROR_INTERNAL_ERROR;
}

ssize_t
keypool_stream_write(keypool_stream_t stream, const json_t *object)
{
	uint8_t nul = '\0';
        ssize_t ret;

	ret = json_dumpb(object,
			 (char *)stream->out + stream->out_end,
			 CONNECTION_BUFFER_SIZE - stream->out_end - 1,
			 JSON_COMPACT);
	if (ret == 0)
		return KEYPOOL_ERROR_BUFFER_TOO_SHORT;

        stream->out_end += ret;
        memcpy(stream->out + stream->out_end, &nul, 1);
        stream->out_end++;

        ret = keypool_stream_flush(stream);
	if (ret < 0)
		return ret;

	return 1;
}
