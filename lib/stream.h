/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_STREAM_H_
#define KEYPOOL_STREAM_H_

#include <keypool/error.h>
#include <gnutls/gnutls.h>
#include <jansson.h>
#include <stdint.h>

typedef struct keypool_stream_st *keypool_stream_t;

struct keypool_stream_st {
        gnutls_session_t session;

        uint8_t *in;
        ptrdiff_t in_start;
        ptrdiff_t in_end;

        uint8_t *out;
        ptrdiff_t out_start;
        ptrdiff_t out_end;
};

keypool_error_t keypool_stream_init(keypool_stream_t stream,
				    gnutls_session_t session);
void keypool_stream_deinit(keypool_stream_t stream);

ssize_t keypool_stream_read(keypool_stream_t stream, json_t **objectp);

ssize_t keypool_stream_write(keypool_stream_t stream, const json_t *object);

#endif	/* KEYPOOL_STREAM_H_ */
