/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_SERVICE_H_
#define KEYPOOL_SERVICE_H_

#include <keypool/credentials.h>

typedef struct keypool_service_st *keypool_service_t;

keypool_error_t keypool_service_new(keypool_service_t *servicep,
				    keypool_credentials_t credentials,
				    const char *pubkey,
				    const char *privkey);
int keypool_service_listen(keypool_service_t service);
int keypool_service_listen_on_address(keypool_service_t service, const char *address);
keypool_error_t keypool_service_dispatch(keypool_service_t service, int fd);
void keypool_service_free(keypool_service_t service);
const char *keypool_service_get_address(keypool_service_t service);
typedef int (*keypool_pin_callback_t)(void *data, const char *prompt,
				      char *pin, size_t pin_size);
void keypool_service_set_pin_function(keypool_service_t service,
				      keypool_pin_callback_t pin_function,
				      void *data);

#endif	/* KEYPOOL_SERVICE_H_ */
