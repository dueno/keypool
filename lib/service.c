/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <keypool/service.h>

#include "basedir.h"
#include "internal.h"
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#include <gnutls/gnutls.h>
#include "message.h"
#include "path.h"
#include "stream.h"
#include <sys/socket.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>

struct client_st
{
	struct keypool_stream_st stream;
	struct client_st *next;
};

struct keypool_service_st
{
	char *pubkey_file;
	gnutls_pubkey_t pubkey;
	char *privkey_file;
	gnutls_privkey_t privkey;
	keypool_pin_callback_t pin_function;
	void *pin_data;
	keypool_credentials_t credentials;
	gnutls_psk_server_credentials_t pskcred;
	char *address;
	struct client_st *clients;
};

void
keypool_service_free(keypool_service_t service)
{
	struct client_st *head, *next;

	if (service == NULL)
		return;

	for (head = service->clients; head != NULL; head = next) {
		keypool_stream_deinit(&head->stream);
		next = head->next;
		free(head);
	}

	free(service->address);
	keypool_credentials_unref(service->credentials);
	if (service->pubkey)
		gnutls_pubkey_deinit(service->pubkey);
	if (service->privkey)
		gnutls_privkey_deinit(service->privkey);
	if (service->pskcred)
		gnutls_psk_free_server_credentials(service->pskcred);
	free(service->pubkey_file);
	free(service->privkey_file);
	
	free(service);
}

static void
service_free_client(keypool_service_t service, int fd)
{
	struct client_st *head = NULL;
	struct client_st **prev = &service->clients;

	while (*prev != NULL) {
		head = *prev;
		if (head->stream.session != NULL &&
		    gnutls_transport_get_int(head->stream.session) == fd) {
			*prev = head->next;
			head->next = NULL;
			keypool_stream_deinit(&head->stream);
			break;
		}
		prev = &head->next;
	}
}

static int
pin_callback (void *userdata, int attempt,
	      const char *token_url,
	      const char *token_label,
	      unsigned int flags,
	      char *pin, size_t pin_max)
{
	keypool_service_t service = userdata;
	const char *prompt_format = "Password for %s: ";
	char *prompt;
	int ret;

	if (service->pin_function == NULL)
		return -1;

	prompt = malloc(strlen(prompt_format) + pin_max + 1);
	if (prompt == NULL)
		return -1;

	sprintf(prompt, prompt_format, service->privkey_file);
	ret = service->pin_function(service->pin_data, prompt, pin, pin_max);
	free(prompt);
	return ret;
}

static keypool_error_t
service_load_keypair(keypool_service_t service,
		     const char *pubkey,
		     const char *privkey)
{
	gnutls_datum_t data;
	int ret;

	ret = gnutls_load_file(privkey, &data);
	if (ret < 0)
		return KEYPOOL_ERROR_FILE_ERROR;

	if (service->pin_function)
		gnutls_privkey_set_pin_function	(service->privkey,
						 pin_callback,
						 service);

	ret = gnutls_privkey_import_x509_raw(service->privkey, &data,
					     GNUTLS_X509_FMT_PEM, NULL, 0);
	gnutls_free(data.data);
	if (ret < 0)
		return KEYPOOL_ERROR_INVALID_FILE;

	if (pubkey == NULL) {
		ret = gnutls_pubkey_import_privkey(service->pubkey,
						   service->privkey,
						   GNUTLS_KEY_DATA_ENCIPHERMENT,
						   0);
		if (ret < 0)
			return KEYPOOL_ERROR_INVALID_FILE;
	} else {
		ret = gnutls_load_file(pubkey, &data);
		if (ret < 0)
			return KEYPOOL_ERROR_FILE_ERROR;
		ret = gnutls_pubkey_import(service->pubkey, &data,
					   GNUTLS_X509_FMT_PEM);
		gnutls_free(data.data);
		if (ret < 0)
			return KEYPOOL_ERROR_INVALID_FILE;
	}

	return 0;
}

static int
pskfunc(gnutls_session_t session, const char *username, gnutls_datum_t *key)
{
	keypool_service_t service;
	struct keypool_buffer_st token = KEYPOOL_BUFFER_INIT;
	keypool_error_t error;

	service = gnutls_session_get_ptr(session);

	error = _keypool_credentials_get(service->credentials,
					 service->privkey,
					 username, &token);
	if (error < 0)
		return -1;
	key->data = gnutls_malloc(token.size);
	memcpy(key->data, token.data, token.size);
	key->size = token.size;
	free(token.data);
	return 0;
}

keypool_error_t
keypool_service_new(keypool_service_t *servicep,
		    keypool_credentials_t credentials,
		    const char *pubkey,
		    const char *privkey)
{
	keypool_service_t service = NULL;
	keypool_error_t error = 0;
	int ret;

	service = calloc(1, sizeof(struct keypool_service_st));
	if (service == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	service->pubkey_file = strdup(pubkey);
	if (service->pubkey_file == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
	service->privkey_file = strdup(privkey);
	if (service->privkey_file == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	ret = gnutls_pubkey_init(&service->pubkey);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
	ret = gnutls_privkey_init(&service->privkey);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	service->credentials = keypool_credentials_ref(credentials);

	ret = gnutls_psk_allocate_server_credentials(&service->pskcred);
	if (ret < 0) {
		ret = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

        gnutls_psk_set_server_credentials_function(service->pskcred, pskfunc);

 out:
	if (error == 0)
		*servicep = service;
	else
		keypool_service_free(service);

	return error;
}

int
keypool_service_listen(keypool_service_t service)
{
	char *basedir;
	char *address;
	keypool_error_t error;

	basedir = keypool_basedir_get_runtime_directory();
	if (basedir == NULL)
		return KEYPOOL_ERROR_UNKNOWN;

	error = keypool_path_build(&address, '/', basedir, "keypool", NULL);
	free(basedir);
	if (error < 0)
		return error;

	error = keypool_service_listen_on_address(service, address);
	free(address);
	return error;
}

int
keypool_service_listen_on_address(keypool_service_t service, const char *address)
{
	struct sockaddr_un sa;
	keypool_error_t error;
	int fd;

	if (strlen(address) > sizeof(sa.sun_path))
		return KEYPOOL_ERROR_BUFFER_TOO_SHORT;

	error = service_load_keypair(service, service->pubkey_file, service->privkey_file);
	if (error < 0)
		return error;

	memset(&sa, 0, sizeof(sa));
	sa.sun_family = AF_UNIX;
	memcpy(sa.sun_path, address, strlen(address));

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
		return KEYPOOL_ERROR_UNKNOWN;

	if (bind(fd, (const struct sockaddr *) &sa, sizeof(sa)) < 0) {
		close(fd);
		return KEYPOOL_ERROR_UNKNOWN;
	}

	if (listen(fd, 20) < 0) {
		close(fd);
		return KEYPOOL_ERROR_UNKNOWN;
	}

	service->address = strdup(address);

	return fd;
}

const char *
keypool_service_get_address(keypool_service_t service)
{
	return service->address;
}

static keypool_error_t
keypool_service_ensure_stream(keypool_service_t service,
			      keypool_stream_t *stream,
			      int fd)
{
	struct client_st *head = NULL;
	gnutls_session_t session = NULL;
	keypool_error_t error = 0;
	int ret;

	for (head = service->clients; head != NULL; head = head->next) {
		if (head->stream.session &&
		    gnutls_transport_get_int(head->stream.session) == fd) {
			*stream = &head->stream;
			return 0;
		}
	}

	head = calloc(1, sizeof(struct client_st));
	if (head == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	ret = gnutls_init(&session, GNUTLS_SERVER);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	ret = gnutls_credentials_set(session, GNUTLS_CRD_PSK, service->pskcred);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

	gnutls_transport_set_int(session, fd);
	gnutls_handshake_set_timeout(session,
				     GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
	gnutls_session_set_ptr(session, service);

	ret = gnutls_priority_set_direct(session, PRIORITY, NULL);
	if (ret < 0) {
		error = KEYPOOL_ERROR_TRANSPORT_ERROR;
		goto out;
	}

        do {
                ret = gnutls_handshake(session);
        } while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

	if (ret < 0) {
		error = KEYPOOL_ERROR_TRANSPORT_ERROR;
		goto out;
	}

	error = keypool_stream_init(&head->stream, session);
	if (error < 0)
		goto out;

	head->next = service->clients;
	service->clients = head;

 out:
	if (error == 0)
		*stream = &head->stream;
	else {
		gnutls_deinit(session);
		free(head);
	}
	return error;
}

static keypool_error_t
keypool_service_handle_get(keypool_service_t service,
			   keypool_stream_t stream,
			   keypool_message_t call)
{
	keypool_message_t reply = NULL;
	keypool_message_t error_reply = NULL;
	const char *username;
	struct keypool_buffer_st token = KEYPOOL_BUFFER_INIT;
	uint8_t *hmac[MAC_SIZE];
	json_t *json;
	uint8_t key[MAX_KEY_SIZE];
	size_t key_size = DEFAULT_KEY_SIZE;
	gnutls_datum_t tmp, buffer;
	gnutls_datum_t previous_key = { NULL, 0 };
	gnutls_datum_t tag = { NULL, 0 };
	size_t size;
	keypool_error_t error = 0;
	ssize_t ret;

	if (keypool_message_get_type(call) != KEYPOOL_MESSAGE_TYPE_CALL)
		return KEYPOOL_ERROR_PROTOCOL_ERROR;

	username = gnutls_psk_server_get_username(stream->session);
	if (username == NULL) {
		error = KEYPOOL_ERROR_INTERNAL_ERROR;
		goto out;
	}
	error = _keypool_credentials_get(service->credentials,
					 service->privkey,
					 username, &token);
	if (error < 0)
		goto out;

	error = keypool_message_read_blob_parameter(call, "tag", NULL, &size);
	if (error < 0 && error != KEYPOOL_ERROR_DATA_NOT_AVAILABLE)
		return error;
	if (error == 0) {
		tmp.data = malloc(size);
		if (tmp.data == NULL)
			return KEYPOOL_ERROR_NO_MEMORY;
		error = keypool_message_read_blob_parameter(call, "tag",
							    tmp.data, &size);
		if (error < 0)
			goto error;
		tmp.size = size;
		if (tmp.size < MAC_SIZE) {
			error = KEYPOOL_ERROR_INVALID_MESSAGE;
			goto out;
		}
		ret = gnutls_hmac_fast(GNUTLS_MAC_SHA256, token.data, token.size,
				       tmp.data, tmp.size - MAC_SIZE, hmac);
		if (ret < 0) {
			error = KEYPOOL_ERROR_DECRYPTION_FAILURE;
			goto error;
		}
		if (memcmp(tmp.data + (tmp.size - MAC_SIZE), hmac, MAC_SIZE) != 0) {
			error = KEYPOOL_ERROR_DECRYPTION_FAILURE;
			goto error;
		}
		tmp.size -= MAC_SIZE;

		ret = gnutls_privkey_decrypt_data(service->privkey, 0,
						  &tmp, &previous_key);
		gnutls_free(tmp.data);
		if (ret < 0) {
			error = KEYPOOL_ERROR_INVALID_MESSAGE;
			goto error;
		}
	}

	ret = keypool_message_read_size_parameter(call, "size");
	if (ret < 0) {
		if (ret != KEYPOOL_ERROR_DATA_NOT_AVAILABLE)
			goto error;
	} else
		key_size = ret;

	/* Export keying material on our side.
	 */
	ret = gnutls_prf_rfc5705(stream->session,
				 sizeof(LABEL) - 1, LABEL,
				 0, NULL,
				 key_size, (char *)key);
	if (ret < 0) {
		error = KEYPOOL_ERROR_TRANSPORT_ERROR;
		goto error;
	}

	tmp.data = key;
	tmp.size = key_size;
	ret = gnutls_pubkey_encrypt_data(service->pubkey, 0, &tmp, &buffer);
	if (ret < 0) {
		error = KEYPOOL_ERROR_TRANSPORT_ERROR;
		goto error;
	}

	ret = gnutls_hmac_fast(GNUTLS_MAC_SHA256, token.data, token.size,
			       buffer.data, buffer.size, hmac);
	if (ret < 0) {
		error = KEYPOOL_ERROR_DECRYPTION_FAILURE;
		goto out;
	}
	buffer.data = gnutls_realloc(buffer.data, buffer.size + MAC_SIZE);
	if (buffer.data == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
	memcpy(buffer.data + buffer.size, hmac, MAC_SIZE);
	buffer.size += MAC_SIZE;

	error = keypool_message_reply_new(&reply);
	if (error < 0)
		goto error;

	error = keypool_message_write_blob_parameter(reply, "tag",
						     buffer.data,
						     buffer.size);
	gnutls_free(tag.data);
	if (error < 0)
		goto error;

	if (previous_key.data != NULL) {
		error = keypool_message_write_blob_parameter(reply, "key",
							     previous_key.data,
							     previous_key.size);
		gnutls_free(previous_key.data);
		if (error < 0)
			goto error;
	}

	error = keypool_message_serialize(reply, &json);
	if (error < 0)
		goto error;

	error = keypool_stream_write(stream, json);
	json_decref(json);

 error:
	if (error < 0 &&
	    (error != KEYPOOL_ERROR_NO_MEMORY ||
	     error != KEYPOOL_ERROR_TRANSPORT_ERROR)) {
		error = keypool_message_error_new(&error_reply,
						  KEYPOOL_NS ".Error");
		if (error < 0)
			return error;
		error = keypool_message_serialize(error_reply, &json);
		if (error < 0)
			goto out;
		error = keypool_stream_write(stream, json);
		json_decref(json);
	}
 out:
	free(token.data);
	keypool_message_free(reply);
	keypool_message_free(error_reply);
	return error;
}

keypool_error_t
keypool_service_dispatch(keypool_service_t service, int fd)
{
	keypool_message_t call = NULL;
	json_t *object;
	keypool_stream_t stream;
	keypool_error_t error = 0;
	ssize_t ret;

	error = keypool_service_ensure_stream(service, &stream, fd);
	if (error < 0) {
		service_free_client(service, fd);
		return error;
	}

	ret = keypool_stream_read(stream, &object);
	if (ret < 0) {
		error = ret;
		service_free_client(service, fd);
		return ret;
	}
	if (ret == 0) {
		error = KEYPOOL_ERROR_TRANSPORT_EOF;
		service_free_client(service, fd);
		goto out;
	}

	error = keypool_message_deserialize(&call, object);
	if (error < 0)
		goto out;

	if (strcmp(keypool_message_get_name(call), KEYPOOL_NS ".Get") == 0)
		error = keypool_service_handle_get(service, stream, call);

 out:
	keypool_message_free(call);

	return error;
}

void
keypool_service_set_pin_function(keypool_service_t service,
				 keypool_pin_callback_t pin_function,
				 void *data)
{
	service->pin_function = pin_function;
	service->pin_data = data;
}
