#include "config.h"

#include <assert.h>
#include <keypool/credentials.h>
#include <jansson.h>
#include <string.h>

static void
test_basic(void)
{
	keypool_credentials_t credentials = NULL;
	json_t *actual, *expected, *actual_token, *expected_token;
	keypool_error_t error;

	error = keypool_credentials_new(&credentials);
	assert(error == 0);
	assert(credentials != NULL);

	error = keypool_credentials_load(credentials,
					 SRCDIR "/fixtures/credentials.json");
	assert(error == 0);

	error = keypool_credentials_save(credentials,
					 BUILDDIR "/credentials.json");
	assert(error == 0);

	actual = json_load_file(BUILDDIR "/credentials.json", 0, NULL);
	assert(actual != NULL);
	assert(json_is_object(actual));

	expected = json_load_file(SRCDIR "/fixtures/credentials.json", 0, NULL);
	assert(expected != NULL);
	assert(json_is_object(expected));

	actual_token = json_object_get(actual, "test");
	assert(json_is_string(actual_token));

	expected_token = json_object_get(actual, "test");
	assert(json_is_string(expected_token));

	assert(json_string_length(actual_token) ==
	       json_string_length(expected_token));
	assert(memcmp(json_string_value(actual_token),
		      json_string_value(expected_token),
		      json_string_length(expected_token)) == 0);

	json_decref(actual);
	json_decref(expected);

	keypool_credentials_ref(credentials);
}

int
main(void)
{
	test_basic();
}
