keypool
======

NOTE: this project is experimental.

keypool is a simple utility to generate and recover data encryption
keys (DEK) for applications.

Usage pattern
------

Initial key generation:

* The application requests a key
* The library returns a newly generated key and an opaque tag
* The application encrypts the data with the key, appends the tag to the ciphertext, discards the key

Subsequent rekeying:

* The library returns the previous key identified by the tag, a newly generated key, and a new tag
* The application decrypts the data with the previous key
* The application encrypts the data with the new key, appends the new tag to the ciphertext, discards the key

Building
------
keypool is built using meson:
```sh
$ meson _build
$ meson test -C _build
```

Command usage
------
Setup the server:
```sh
$ certtool --generate-privkey --key-type RSA --pkcs8 --outfile privkey.pem
Enter password: ...
$ certtool --load-privkey privkey.pem --pubkey-info --outfile pubkey.pem
Enter password: ...
$ ./kpcred register --credentials credentials.json --pubkey pubkey.pem --name «name»
«name» is registered with token: «token»
```

Start the test server:
```sh
$ ./kpserv --credentials credentials.json --pubkey pubkey.pem --privkey privkey.pem
Password for privkey.pem:
```

Generate initial key with the test client:
```sh
$ ./kpcli --name «name» --token «token»
Key: «key»
Tag: «tag»
```

Key recovery and rekey:
```sh
$ ./kpcli --name «name» --token «token» --tag «tag»
Previous Key: «key»
Previous Tag: «tag»
Key: «keyʹ»
Tag: «tagʹ»
```

How it works
------

keypool consists of a server and a client communicating through a
local Unix socket.  The communication is protected with TLS.  The
authentication is PSK.

Once the connection is established, both peers export a shared keying
material.  This is the key returned to the application.

At the same time, the server encrypts the keying material with its
private key, calculates a MAC over the ciphertext using the client's
PSK as a key, and sends it to the client.  This is the tag returned to
the application.

On a subsequent rekeying, the server verifies and decrypts the tag,
and sends it to the client (as the previous key) along with a new key
and tag.

License
------
* library: LGPLv3+ or GPLv2+
* programs: GPLv3+

```
Copyright (C) 2019 Daiki Ueno
Copyright (C) 2019 Red Hat, Inc.

This file is free software; as a special exception the author gives
unlimited permission to copy and/or distribute it, with or without
modifications, as long as this notice is preserved.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```
