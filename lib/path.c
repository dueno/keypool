/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "path.h"

#include <assert.h>
#include <keypool/error.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

keypool_error_t
keypool_path_build(char **result,
		   int delim,
		   const char *path,
		   ...)
{
	const char *first = path;
	char *built;
	size_t len;
	size_t at;
	size_t num;
	size_t until;
	va_list va;

	len = 1;
	va_start(va, path);
	while (path != NULL) {
		size_t old_len = len;
		len += strlen(path) + 1;
		if (len < old_len) {
			va_end(va);
			return KEYPOOL_ERROR_BUFFER_TOO_SHORT;
		}
		path = va_arg(va, const char *);
	}
	va_end(va);

	built = malloc(len + 1);
	if (built == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	at = 0;
	path = first;
	va_start(va, path);
	while (path != NULL) {
		num = strlen(path);

		/* Trim end of the path */
		until = (at > 0) ? 0 : 1;
		while (num > until && path[num - 1] == delim)
			num--;

		if (at != 0) {
			if (num == 0)
				continue;
			built[at++] = delim;
		}

		assert(at + num < len);
		memcpy(built + at, path, num);
		at += num;

		path = va_arg(va, const char *);

		/* Trim beginning of path */
		while (path && path[0] == delim)
			path++;
	}
	va_end(va);

	assert(at < len);
	built[at] = '\0';
	*result = built;
	return 0;
}
