/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_BUFFER_H_
#define KEYPOOL_BUFFER_H_

#include <keypool/error.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

struct keypool_buffer_st {
	uint8_t *data;
	size_t size;
	size_t capacity;
	bool allocated;
};

#define KEYPOOL_BUFFER_INIT { NULL, 0, 0, 0 }

typedef struct keypool_buffer_st *keypool_buffer_t;

keypool_error_t keypool_buffer_init(keypool_buffer_t buffer,
				    const uint8_t *data, size_t size);
void keypool_buffer_init_borrowed(keypool_buffer_t buffer,
				  uint8_t *data, size_t size);
keypool_error_t keypool_buffer_init_sized(keypool_buffer_t buffer,
					 size_t size);
keypool_error_t keypool_buffer_set(keypool_buffer_t buffer,
				   const uint8_t *data, size_t size);
const uint8_t *keypool_buffer_get(keypool_buffer_t buffer, size_t *size);
void keypool_buffer_deinit(keypool_buffer_t buffer);

#endif	/* KEYPOOL_BUFFER_H_ */
