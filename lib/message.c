/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "message.h"

#include <gnutls/gnutls.h>
#include <stdlib.h>
#include <string.h>

struct keypool_message_st {
	keypool_message_type_t type;
	char *name;		/* the value of "method" or "error" */
	json_t *parameters;
};

static keypool_error_t
message_new(keypool_message_t *messagep,
	    keypool_message_type_t type)
{
	keypool_message_t message;
	keypool_error_t error = 0;

	message = calloc(1, sizeof(struct keypool_message_st));
	if (message == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	message->type = type;
	message->parameters = json_object();
	if (message->parameters == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

 out:
	if (error == 0)
		*messagep = message;
	else
		keypool_message_free(message);
	return error;
}

static keypool_error_t
message_new_with_name(keypool_message_t *messagep,
		      keypool_message_type_t type,
		      const char *name)
{
	keypool_message_t message;
	keypool_error_t error = 0;

	error = message_new(&message, type);
	if (error < 0)
		return error;

	message->name = strdup(name);
	if (message->name == NULL) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}

 out:
	if (error == 0)
		*messagep = message;
	else
		keypool_message_free(message);
	return error;
}

keypool_error_t
keypool_message_call_new(keypool_message_t *messagep, const char *name)
{
	return message_new_with_name(messagep, KEYPOOL_MESSAGE_TYPE_CALL, name);
}

keypool_error_t
keypool_message_reply_new(keypool_message_t *messagep)
{
	return message_new(messagep, KEYPOOL_MESSAGE_TYPE_REPLY);
}

keypool_error_t
keypool_message_error_new(keypool_message_t *messagep, const char *name)
{
	return message_new_with_name(messagep, KEYPOOL_MESSAGE_TYPE_ERROR, name);
}

keypool_message_type_t
keypool_message_get_type(keypool_message_t message)
{
	return message->type;
}

const char *
keypool_message_get_name(keypool_message_t message)
{
	return message->name;
}

void
keypool_message_free(keypool_message_t message)
{
	if (message != NULL) {
		free(message->name);
		json_decref(message->parameters);
	}
	free(message);
}

keypool_error_t
keypool_message_write_size_parameter(keypool_message_t message,
				     const char *name,
				     size_t size)
{
	json_t *value;
	keypool_error_t error = 0;
	int ret;

	value = json_integer(size);
	if (value == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	ret = json_object_set_new(message->parameters, name, value);
	if (ret < 0) {
		json_decref(value);
		error = KEYPOOL_ERROR_NO_MEMORY;
	}

	return error;
}

ssize_t
keypool_message_read_size_parameter(keypool_message_t message,
				    const char *name)
{
	json_t *value;

	value = json_object_get(message->parameters, name);
	if (value == NULL)
		return KEYPOOL_ERROR_DATA_NOT_AVAILABLE;
	if (!json_is_integer(value))
		return KEYPOOL_ERROR_INVALID_MESSAGE;

	return json_integer_value(value);
}

keypool_error_t
keypool_message_write_blob_parameter(keypool_message_t message,
				     const char *name,
				     const uint8_t *blob, size_t blob_size)
{
	gnutls_datum_t tmp, buffer;
	json_t *value;
	keypool_error_t error = 0;
	int ret;

	tmp.data = (uint8_t *)blob;
	tmp.size = blob_size;
	ret = gnutls_base64_encode2(&tmp, &buffer);
	if (ret < 0)
		return KEYPOOL_ERROR_NO_MEMORY;

	value = json_string((char *)buffer.data);
	gnutls_free(buffer.data);
	if (value == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	ret = json_object_set_new(message->parameters, name, value);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		json_decref(value);
	}

	return error;
}

keypool_error_t
keypool_message_read_blob_parameter(keypool_message_t message,
				    const char *name,
				    uint8_t *blob, size_t *blob_size)
{
	gnutls_datum_t tmp, buffer;
	json_t *value;
	int ret;

	value = json_object_get(message->parameters, name);
	if (value == NULL)
		return KEYPOOL_ERROR_DATA_NOT_AVAILABLE;
	if (!json_is_string(value))
		return KEYPOOL_ERROR_INVALID_MESSAGE;

	if (blob == NULL) {
		*blob_size = json_string_length(value) / 4 * 3;
		return 0;
	}

	if (json_string_length(value) / 4 * 3 > *blob_size)
		return KEYPOOL_ERROR_INVALID_MESSAGE;

	tmp.data = (uint8_t *)json_string_value(value);
	tmp.size = json_string_length(value);
	ret = gnutls_base64_decode2(&tmp, &buffer);
	if (ret < 0)
		return KEYPOOL_ERROR_INVALID_MESSAGE;

	if (*blob_size < buffer.size)
		return KEYPOOL_ERROR_INTERNAL_ERROR;

	memcpy(blob, buffer.data, buffer.size);
	*blob_size = buffer.size;
	gnutls_free(buffer.data);

	return 0;
}

keypool_error_t
keypool_message_serialize(keypool_message_t message, json_t **json)
{
	json_t *root;
	json_t *value = NULL;
	keypool_error_t error = 0;
	int ret;

	root = json_object();
	if (root == NULL)
		return KEYPOOL_ERROR_NO_MEMORY;

	switch (message->type) {
	case KEYPOOL_MESSAGE_TYPE_CALL:
		value = json_string(message->name);
		if (value == NULL) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			goto out;
		}
		ret = json_object_set_new(root, "method", value);
		if (ret < 0) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			goto out;
		}
		break;
	case KEYPOOL_MESSAGE_TYPE_ERROR:
		value = json_string(message->name);
		if (value == NULL) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			goto out;
		}
		ret = json_object_set_new(root, "error", value);
		if (ret < 0) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			goto out;
		}
		break;
	case KEYPOOL_MESSAGE_TYPE_REPLY:
		break;
	default:
		error = KEYPOOL_ERROR_INVALID_MESSAGE;
		goto out;
	}

	ret = json_object_set(root, "parameters", message->parameters);
	if (ret < 0) {
		error = KEYPOOL_ERROR_NO_MEMORY;
		goto out;
	}
 out:
	if (error == 0)
		*json = root;
	else {
		json_decref(value);
		json_decref(root);
	}
	return error;
}

keypool_error_t
keypool_message_deserialize(keypool_message_t *messagep, json_t *json)
{
	keypool_message_t message;
	json_t *value;
	keypool_error_t error = 0;
	int ret;

	if (!json_is_object(json))
		return KEYPOOL_ERROR_INVALID_MESSAGE;

	value = json_object_get(json, "method");
	if (value != NULL && json_is_string(value)) {
		error = keypool_message_call_new(&message, json_string_value(value));
		if (error < 0)
			goto out;
	} else {
		value = json_object_get(json, "error");
		if (value != NULL && json_is_string(value)) {
			error = keypool_message_error_new(&message, json_string_value(value));
			if (error < 0)
				goto out;
		} else {
			error = keypool_message_reply_new(&message);
			if (error < 0)
				goto out;
		}
	}

	value = json_object_get(json, "parameters");
	if (value != NULL) {
		ret = json_object_update(message->parameters, value);
		if (ret < 0) {
			error = KEYPOOL_ERROR_NO_MEMORY;
			goto out;
		}
	}

 out:
	if (error == 0)
		*messagep = message;
	else
		keypool_message_free(message);
	return error;
}
