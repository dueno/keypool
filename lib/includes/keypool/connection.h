/*
 * Copyright (C) 2019 Daiki Ueno
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * This file is part of keypool.
 *
 * keypool is free software: you can redistribute it and/or
 * modify it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at your
 *    option) any later version.
 *
 * or
 *
 *  * the GNU General Public License as published by the Free
 *    Software Foundation; either version 2 of the License, or (at your
 *    option) any later version.
 *
 * or both in parallel, as here.
 *
 * keypool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and
 * the GNU Lesser General Public License along with this program.  If
 * not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYPOOL_CONNECTION_H_
#define KEYPOOL_CONNECTION_H_

#include <keypool/buffer.h>

typedef struct keypool_connection_st *keypool_connection_t;

keypool_error_t keypool_connection_new(keypool_connection_t *connection,
				       const char *name,
				       const keypool_buffer_t token);
keypool_error_t keypool_connection_new_for_address(keypool_connection_t *connection,
						   const char *name,
						   const keypool_buffer_t token,
						   const char *address);
void keypool_connection_free(keypool_connection_t connection);
keypool_error_t keypool_connection_rekey(keypool_connection_t connection,
					 keypool_buffer_t key,
					 keypool_buffer_t previous_key,
					 keypool_buffer_t tag);

#endif	/* KEYPOOL_CONNECTION_H_ */
